import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { WorkoutService } from '../../services/workout.service';
import { WorkoutsPage } from '../workouts/workouts';

@Component({
  selector: 'page-workout-details',
  templateUrl: 'workout-details.html',
  providers: [ WorkoutService ]
})
export class WorkoutDetailsPage {

  workout: any;
  result: string;

  constructor(public nav: NavController, public navParams: NavParams, public workoutService: WorkoutService) {
  	this.nav = nav;
  	this.navParams = navParams;
  	this.workout = this.navParams.get('workout');
  }

  deleteWorkout(workoutId){
  	// Delete Workout
  	this.workoutService.deleteWorkout(workoutId).subscribe(data => {
  		this.result = data
  	}, 
  	err => console.log(err), 
  	() => console.log('Workout Deleted'));

  	this.nav.setRoot(WorkoutsPage);
  }

}
