//import {Injectable, Inject} from '@angular/core';
import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
//import {Http, Headers, RequestOptions} from '@angular/http';
//import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class WorkoutService{
	apiKey: string;
	workoutsUrl: string;
	
	constructor(public http: Http){
		this.apiKey = '0l-FxWg34fUZWH6eXP68joR5P3I0u4nB';
		this.workoutsUrl = 'https://api.mlab.com/api/1/databases/myworkoutsapp/collections/workouts';
	}

	getWorkouts(){
		return this.http.get(this.workoutsUrl + '?apiKey=' + this.apiKey)
			.map( res => res.json());
	}

	addWorkout(workout){
		var headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.post(this.workoutsUrl+'?apiKey=' + this.apiKey, JSON.stringify(workout), 
			{headers: headers})
			.map( res => res.json());
	}

	deleteWorkout(workoutId){
		return this.http.delete(this.workoutsUrl + '/'+workoutId+'?apiKey=' + this.apiKey)
			.map( res => res.json());
	}


}