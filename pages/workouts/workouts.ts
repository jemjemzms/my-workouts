import { Component, OnInit} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { WorkoutService } from '../../services/workout.service';
import { WorkoutDetailsPage} from '../workout-details/workout-details';

@Component({
	selector: 'page-workouts',
	templateUrl: 'workouts.html',
	providers: [ WorkoutService ]
})
export class WorkoutsPage implements OnInit{
  workouts: string;

  constructor(public nav: NavController, public navParams: NavParams, public workoutService: WorkoutService) {
    this.nav = nav;
  	this.workoutService = workoutService;
    this.workouts;
    
    this.workoutService.getWorkouts().subscribe(workouts =>{
      this.workouts = workouts;
    });
  }

  ngOnInit(){
  	this.workoutService.getWorkouts().subscribe(workouts =>{
  		this.workouts = workouts;
  	});
  }

  workoutSelected(event, workout){
    this.nav.push(WorkoutDetailsPage, {
      workout: workout
    });
  }
}